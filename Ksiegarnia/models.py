from django.db import models

# Create your models here.
class Ksiazka(models.Model):
    author_name= models.CharField(max_length=200)
    title= models.CharField(max_length=200)
    category=models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
